// The build function is responsible for the initial creation of the different component objects.

function build(settingsManager, helperFunctionManager, additionalObjectsManager) {
    const sAll = settingsManager;
    const f = helperFunctionManager;
    const o = additionalObjectsManager;

    // Create components
    const components = {
        componenta: componentA(sAll.general, sAll.componenta, f)
    };

    return components;
}