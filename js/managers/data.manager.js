// The data manager loads, stores, and filters the data 

class DataManager{
    
    constructor(settingsManager) {
        this.sAll = settingsManager;
        this.data = {};
        this.fileList = [
            {name: "textData", path: "/data/text_data.csv"}
        ]
    }

    rowConverter = function(d, i) {
        return {idx: +d.idx, message: d.message};
    }

    loadData = function() {
        return Promise.all(this.fileList.map(d => d3.csv(d.path, this.rowConverter)))
    }

    addData = function(dataNew) {
        // Convert array of data to more easily accessible object
        this.fileList.map((d, i) => {
            this.data[d.name] = dataNew[i];
        })
    }
    
    filterTextData = function(filterData, searchTerm) {
        return filterData.filter(d => {
            return d.message === searchTerm;
        })
    };
}