// In the layout manager, all changes to the overall appearance of the app are managed. 
// The initial creation of the different containers, however, happens inside "index.html".

class LayoutManager{
    
    constructor(settingsManager) {
        this.sAll = settingsManager;
        this.setupButtons();
    }

    setupButtons = function() {
        d3.select('#container-toolbar-component-a')
            .append('button')
            .attr("id", "random-button")
            .attr("class", "tool-button")
            .text("Random");
        d3.select('#container-toolbar-component-a')
            .append('button')
            .attr("id", "reset-button")
            .attr("class", "tool-button")
            .text("Reset");
    };

    addDataToComponents = function(data, components) {
        // Init SVG
        const svgComponentA = d3.select('#container-component-a')
            .selectAll("#svg-component-a")
            .data([0])
            .join(
                enter => enter
                    .append('svg:svg')
                    .attr('id', 'svg-component-a')
                    .style("position", "absolute")
            );

        // Draw component
        svgComponentA.data([data.textData]).call(components.componenta);
    };
    
    toggleVisibility = function(containerName) {
        const container = d3.select('#'+containerName)
        if (container.style('display') === 'none') {
            container.style('display', 'block');
        }
        else {
            container.style('display', 'none');
        }
    };
}