// The events manager creates the event listeners and defines the functions which are called in response to the different events.

class EventManager {

    addListeners = function() {
        d3.select('#container-toolbar-component-a').select('#random-button')
            .on('click', this.events.randomizePositions);
        d3.select('#container-toolbar-component-a').select('#reset-button')
            .on('click', this.events.resetData);
    }

    constructor(sAll, dataManager, c) {

        this.events = {
            randomizePositions: function() {
                const s = sAll.componenta;
                c.componenta.x(Math.random()*s.offset);
                c.componenta.y(Math.random()*s.offset);
                let dataFiltered = d3.select('#svg-component-a').datum();
                if (Math.round(Math.random())) {
                    dataFiltered = dataManager.filterTextData(dataFiltered, "message A")
                }
                d3.select('#svg-component-a').data([dataFiltered]).call(c.componenta.update);
            },

            resetData: function() {
                d3.select('#svg-component-a').data([dataManager.data.textData]).call(c.componenta.update);
            }
        }
    }

}