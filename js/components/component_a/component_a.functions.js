// Here, all the internal helper functions from component_a are stored to keep the script for the component as slim as possible.
// Since sg, s, si, and f are passed, the functions can be written exactly the same as they would be inside the main script of component_a.component.js.

function getComponentAFunctions(sg, s, si, f) {
    const fi = {};

    fi.getXPos = function(i) {
        return s.offset + s.x*i;
    }

    fi.getYPos = function(i) {
        return s.offset + s.y*i;
    }

    return fi;
} 

