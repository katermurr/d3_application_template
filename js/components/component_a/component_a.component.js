//The structure of components follows mainly the one described in https://bost.ocks.org/mike/chart/

function componentA(sg, s, f) {
    /*
    "s" is used for the "settings" from the settingsManager corresponding to component_a,
    "sg" is used for the general settings from the settingsManager,
    and "si" is used for the internal settings that are not to be accessed from the outside.
    Correspondingly, "f" are the general helper functions while "fi" are the internal helper functions.
    The internal helper functions where moved to an external script to keep the code as slim as possible.
    */
    const si = {};
    const fi = getComponentAFunctions(sg, s, si, f);

    /*
    For development, append variables to s and f here and move them to the respective managers later
    s.width = 500;
    ...
    */

    function component(selection) {
        // Create permanent stuff here and anything that potentially needs to be updated in "component.update"
        // This function is only called when initially building the component, later on only the update function will be called. 
        selection.each(function(data, idx) {
            if (!data) {return;}
            // Select the svg element
            si.svg = d3.select(this)
                .attr('width', s.width)
                .attr('height', s.height);
            // Append group for the content of the svg
            si.gAll = si.svg.append('g')
                .attr('class', 'component-a-content');
        })

        // Update DOM
        component.update(selection);
    }

    component.update = function(selection) {
        selection.each(function(data, idx) {
            if (!data) {return;}

            si.gAll.selectAll('text')
                .data(data, d => d.idx)
                .join(
                    enter => enter
                        .append('text')
                        .attr('x', (d,i) => fi.getXPos(i))
                        .attr('y', (d,i) => fi.getYPos(i))
                        .text(d => d.message),
                    update => update
                        .attr('x', (d,i) => s.offset + s.x*i)
                        .attr('y', (d,i) => s.offset + s.y*i),
                    remove => remove.remove()
                );
        })
    }

    // The getters and setters described in #Reconfiguration from https://bost.ocks.org/mike/chart/ 
    f.createGettersSetters(component, s);

    return component;
}